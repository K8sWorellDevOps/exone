<?php 
    # Suchfunktion in der Gallerie Datenbank nach Treffern
    include "includes/db.php";
    include "includes/header.php"; 
    
?>
    <div class="container">
        <div class="row">
            <!-- Blog Entries Column -->
            <div class="col-md-8">
            <?php 
                if(isset($_POST['submit']))
                {
                    $search = $_POST['search'];
                    $query = "SELECT * FROM galerie WHERE comment LIKE '%$search%' ";
                    $search_query = mysqli_query($connection, $query);

                    if(!$search_query)
                    {
                        die("Query fehlgeschlagen" . mysqli_error($connection));
                    }

                    $count = mysqli_num_rows($search_query);

                    if($count == 0)
                    {
                        echo "<h1>Kein Ergebnisse gefunden</h1>";
                    }else{
                        $select_all_posts = mysqli_query($connection, $query);
                        while ($row = mysqli_fetch_assoc($search_query))
                        {
                            $comment_id = $row['id'];
                            $comment = $row['comment'];
                            $image = $row['image'];
                    ?>
                            <?php echo '<img width="150px" class="modal_thumbnails img-responsive" src="data:image/jpeg;base64,'.base64_encode($image).'">'; ?>
                            <hr>
                            <p><?= $comment; ?></p>
                            <a class="btn btn-primary" href="#">Mehr erfahren <span class="glyphicon glyphicon-chevron-right"></span></a>
                            <hr>
                    <?php 
                        }
                    }
                }
                       
            ?>
    
            </div>
           
            </div>
        </div>
        <hr>
</body>

</html>

