<?php  

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

include "includes/db.php"; 
include "includes/header.php"; 
include "admin/function.php"; 
require_once('classes/Notification.php');
require "vendor/autoload.php";

$admin_mail = new Notification();


$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

$options = array(
    'cluster' => 'eu',
    'encrypted' => true
);

$pusher = new Pusher\Pusher(getenv('APP_KEY'), getenv('APP_SECRET'), getenv('APP_ID'), $options);


if(!ifItIsMethod('get') && !isset($_GET['forgot']))
{
    redirect('index.php');
}

if(ifItIsMethod('post'))
{
    if(isset($_POST['email']))
    {
        $email = $_POST['email'];
        $length = 50;
        $token = bin2hex(openssl_random_pseudo_bytes($length));
        
        if(email_exist($email))
        {
            $stmt = mysqli_prepare($connection, "UPDATE users SET token='{$token}' WHERE email=?"); 
            mysqli_stmt_bind_param($stmt, "s", $email);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);

            /*
            *   PHPMailer Konfiguration
            */

            try{
                $mail = new PHPMailer(true);
                $message = '<p>Hier klicken, um das Passwort zurückzusetzen

               <a href="http://192.168.178.23/original/reset.php?email='.$email.'&token='.$token.' ">http://192.168.178.23/original/reset.php?email='.$email.'&token='.$token.'</a>

                </p>';
                $mail->isHTML(true);  
                $mail->AddAddress($email, "Tobias");
                $mail->SetFrom($email, "EXONE - Das Smart-Home");
                $mail->subject = $subject; 
                $mail->Body    = $message;
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $subject = "This is a test email";

                $mail->send();
                echo "<p class='alert alert-success' role='alert' style='text-align: center;'><strong>Sehr schön! </strong>Die E-Mail wurde versandt</p>";
                $admin_mail->send_email("exone.contact@gmail.com", "Passwort vergessen -  Hinweis", "Ein Benutzer hat sein Passwort geändert.");
                $data['message'] = $email;
                $pusher->trigger('forgot', 'forgot_passwort', $data);

            }catch (Exception $e) {
                echo "<p class='alert alert-danger' role='alert' style='text-align: center;'><strong>ACHTUNG! </strong>E-Mail konnte nicht gesendet werden. Mailer Fehler:</p> ", $mail->ErrorInfo;
            }
        }else{
            echo "<p class='alert alert-danger' role='alert' style='text-align: center;'>Du bist leider nicht befugt dein Passwort zurückzusetzen. <a href='index.php'>zurück</a></p>";
         }
    }
}?>

<div class="container">
    <div class="form-gap"></div>
    <div class="container">
        <h1></h1><br><br><br>
        <div class="row">
            <div class="col-md-66 mx-auto">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">

                                <h3><i class="fa fa-lock fa-4x"></i></h3>
                                <h2 class="text-center">Passwort vergessen?</h2>
                                <p>Hier kannst du dein Passwort zurücksetzen.</p>
                                <div class="panel-body">

                                    <form id="register-form" role="form" autocomplete="off" class="form" method="post">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
                                                <input id="email" name="email" placeholder="E-Mail Adresse" class="form-control"  type="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input name="recover-submit" class="btn btn-lg btn-primary btn-block" value="Passwort zurücksetzen" type="submit">
                                        </div>
                                        <input type="hidden" class="hide" name="token" id="token" value="">
                                    </form>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

