$(document).ready(function(){
    var div_box = "<div id='load-screen'><div id='loading'></div></div>";
    $("body").prepend(div_box);
    $('#load-screen').delay(700).fadeOut(600, function(){
        $(this).remove();
    });

    $(".modal_thumbnails").click(function(){
        $("#set_user_image").prop('disabled', false);
    });
});

function loadUsersOnline()
{
    $.get("function.php?onlineusers=result", function(data){
        $(".usersonline").text(data);
    });
}

setInterval(function(){
    loadUsersOnline();
}, 500);


