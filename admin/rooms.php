<?php 
    include "includes/admin_header.php";
?>
    <div id="wrapper">
    <?php  include "includes/admin_nav.php"; ?>
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><br><br><br>
                        Räume
                        <small>Übersicht</small>
                    </h1>
                                <?php 
                                    if(isset($_GET['source']))
                                    {
                                        $source = $_GET['source'];
                                    }else{
                                        $source = '';
                                    }
                                    switch($source)
                                    {
                                        case 'add_room';
                                            include 'includes/add_room.php';
                                        break;
                                        case 'edit_rooms';
                                            include "includes/edit_room.php";
                                        break;
                                        default: 
                                            include "includes/view_all_rooms.php";
                                        break;
                                    }
                                ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</body>
<?php include "includes/admin_footer.php"; ?>
</html>