<?php
    function imagePlaceholder($image=null)
    {
        if(!$image)
        {
            return 'standart.png';
        }else{
            return $image;
        }
    }

    function redirect($location)
    {
        return header("Location:" . $location);
        exit;
    }

    function users_online()
    {
        if(isset($_GET['onlineusers']))
        {
            global $connection;

            if(!$connection)
            {
                session_start();
                include("../includes/db.php");

                $session = session_id();
                $time = time();
                $time_out_in_seconds = 30;
                $time_out = $time - $time_out_in_seconds;

                $query = "SELECT * FROM users_online WHERE session = '$session' ";
                $send_query = mysqli_query($connection, $query);
                $count = mysqli_num_rows($send_query);

                if($count == NULL)
                {
                    mysqli_query($connection, "INSERT INTO users_online(session, time) VALUES('$session','$time') ");
                }else{
                    mysqli_query($connection, "UPDATE users_online SET time = '$time' WHERE session = '$session' ");
                }
                $user_online_query = mysqli_query($connection, "SELECT * FROM users_online WHERE time > '$time_out' ");
                echo $count_user = mysqli_num_rows($user_online_query);
            }
        }
    }

    users_online();

    function getSetValue($value)
    {
        echo $value;
    }

    function confirm($result)
    {
        global $connection;
        
        if(!$result)
        {
            die("Query failed!").mysqli_error($connection);
        }
    }
    function insert_categories()
    {
        global $connection;

        if(isset($_POST['submit']))
                        {
                            $title = $_POST['title'];

                            if($title == "" || empty($title))
                            {
                                echo "<div class='alert alert-danger' role='alert'>Das Feld darf nicht leer sein.</div>";
                            }else{
                                $query = "INSERT INTO categories(title)";
                                $query .= "VALUE('{$title}')";
                                $create_category_query = mysqli_query($connection, $query);

                                if(!$create_category_query)
                                {
                                    die("<div class='alert alert-danger' role='alert'>Query fehlgeschlagen</div>" . mysql_error($connection));
                                }
                                echo "<div class='alert alert-success' role='alert'>Kategorie wurde erfolgreich erstellt.</div>";
                            }
                        }
    }

    function findAllCategories()
    {
         global $connection;
         // Finde alle Kategorien
         $query = "SELECT * FROM categories";
         $select_categories = mysqli_query($connection, $query);
         
         while ($row = mysqli_fetch_assoc($select_categories))
         {
             $id = $row['id'];
             $title = $row['title'];

             echo "<tr>";
             echo "<td class='alert alert-danger'>{$id}</td>";
             echo "<td class='alert alert-info'>{$title}</td>";
             echo "<td class='alert alert-warning'><a href='categories.php?edit={$id}' class='alert alert-primary'>bearbeiten</a></td>";
             echo "<td class='alert alert-warning'><a href='categories.php?delete={$id}' class='alert alert-danger'>Löschen</a></td>";
             echo "</tr>";
         }
    }

    function delete_categories()
    {
        global $connection;

        if (isset($_GET['delete']))
         {
            $cat_id = $_GET['delete'];

            $query = "DELETE FROM categories WHERE  id = {$cat_id}";
            $delete_query = mysqli_query($connection, $query);
            header("location: categories.php");
         }
    }


    function register_user($username, $email, $passwort)
    {
        global $connection; 

        if(!empty($username) && !empty($email) && !empty($passwort)) {
            
            $username = mysqli_real_escape_string($connection, $username);
            $email = mysqli_real_escape_string($connection, $email);
            $passwort = mysqli_real_escape_string($connection, $passwort);

            $query = "SELECT randSalt FROM users";

            $select_randsalt_query = mysqli_query($connection, $query);

            if(!$select_randsalt_query)
            {
                die("Fehlgeschlagen" . mysqli_error($connection));
            }

            $row = mysqli_fetch_array($select_randsalt_query);
            $salt = $row['randSalt'];
            $passwort = crypt($passwort, $salt);

            $query = "INSERT INTO users (name, email, password, user_role)";
            $query .= "VALUES('{$username}', '{$email}','{$passwort}', 'subscriber')";
            $register_user_query = mysqli_query($connection, $query);

            if(!$register_user_query)
            {
                die("Fehlgeschlagen 2" . mysqli_error($connection) . '' . mysqli_errno($connection));
            }

            echo $message2; 

            $message = "<div class='alert alert-success' role='alert' style='text-align: center;'><strong>Sehr schön!</strong> Du bist jetzt registriert.</div>";

        }else{

            $message2 = ""; 
            $message = "<div class='alert alert-danger' role='alert' style='text-align: center;'><strong>ACHTUNG!</strong> Die Felder dürfen nicht leer sein.</div>";
        }
    }

    function login_user($email, $password)
    {
        global $connection; 

        $email = trim($email);
        $password = trim($password);

        $email = mysqli_real_escape_string($connection, $email);
        $password = mysqli_real_escape_string($connection, $password);

        $query = "SELECT * FROM users WHERE email = '{$email}' ";
        $select_user_query = mysqli_query($connection, $query);

        if(!$select_user_query)
        {
            die("Query failed" . mysqli_error($connection));
        }

        while($row = mysqli_fetch_array($select_user_query))
        {
            $db_id = $row['id'];
            $db_name = $row['name'];
            $db_bday = $row['bday'];
            $db_home = $row['home'];
            $db_tel = $row['tel'];
            $db_email = $row['email'];
            $db_pass = $row['password'];
            $db_role = $row['user_role'];
        }

        $password = crypt($password, $db_pass);

        if($email === $db_email && $password === $db_pass)
        {
            $_SESSION['email'] = $db_email;
            $_SESSION['name'] = $db_name;
            $_SESSION['bday'] = $db_bday;
            $_SESSION['tel'] = $db_tel;
            $_SESSION['user_role'] = $db_role;
            
            redirect("../admin");
        }else{
            redirect("../index.php");
        }
    }

    function ifItIsMethod($method=null)
    {
        if($_SERVER['REQUEST_METHOD'] == strtoupper($method))
        {
            return true;
        }
        return false;
    }

    function isLoggedIn()
    {
        if(isset($_SESSION['user_role']))
        {
            return true;
        }
        return false;
    }

    function checkIfUserLoggedInAndRedirect($redirectLocation=null)
    {
        if(isLoggedIn())
        {
            redirect($redirectLocation);
        }
    }

    function is_admin($username = "")
    {
        global $connection;

        $query = "SELECT name FROM users WHERE name = '$username'";

        $result = mysqli_query($connection, $query);
        $row = mysqli_fetch_array($result);

        if($row['user_role'] == 'admin')
        {
            return true; 
        }else{
            return false;
        }
    }

    function username_exist($username)
    {
        global $connection; 

        $query = "SELECT name FROM users WHERE name = '$username'";

        $result = mysqli_query($connection, $query);

        if(mysqli_num_rows($result) > 0)
        {
            return true;
        }else{
            return false; 
        }
    }

    function email_exist($email)
    {
        global $connection; 

        $query = "SELECT email FROM users WHERE email = '$email'";

        $result = mysqli_query($connection, $query);

        if(mysqli_num_rows($result) > 0)
        {
            return true;
        }else{
            return false; 
        }
    }

    function currentUser()
    {
        if(isset($_SESSION['name']))
        {
            return $_SESSION['name'];
        }
    }

?>