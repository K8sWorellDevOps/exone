<?php
    require_once "../../../../classes/Notification.php"; 
    include "../../function.php";
    include "../header.php";
    require '../../../../vendor/autoload.php'; 

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Gmail API PHP Quickstart');
    $client->setScopes(Google_Service_Gmail::GMAIL_READONLY);
    $client->setAuthConfig('credentials.json');
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = 'token.json';
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
            throw new Exception(join(', ', $accessToken));
        }

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}


// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Gmail($client);

// Print the labels in the user's account.
$user = 'me';
$results = $service->users_labels->listUsersLabels($user);


?>


 <?php include "nav.php"; 
 // Authentication things above
/*
 * Decode the body.
 * @param : encoded body  - or null
 * @return : the body if found, else FALSE;
 */
function decodeBody($body) {
    $rawData = $body;
    $sanitizedData = strtr($rawData,'-_', '+/');
    $decodedMessage = base64_decode($sanitizedData);
    if(!$decodedMessage){
        $decodedMessage = FALSE;
    }
    return $decodedMessage;
}

$client2 = getClient();
$gmail = new Google_Service_Gmail($client2);

$list = $gmail->users_messages->listUsersMessages('me', ['maxResults' => 1000]);

try{
    while ($list->getMessages() != null) {

        foreach ($list->getMessages() as $mlist) {

            $message_id = $mlist->id;
            $optParamsGet2['format'] = 'full';
            $single_message = $gmail->users_messages->get('me', $message_id, $optParamsGet2);
            $payload = $single_message->getPayload();
            $parts = $payload->getParts();
            // With no attachment, the payload might be directly in the body, encoded.
            $body = $payload->getBody();
            $FOUND_BODY = FALSE;
            // If we didn't find a body, let's look for the parts
            if(!$FOUND_BODY) {
                foreach ($parts  as $part) {
                    if($part['parts'] && !$FOUND_BODY) {
                        foreach ($part['parts'] as $p) {
                            if($p['parts'] && count($p['parts']) > 0){
                                foreach ($p['parts'] as $y) {
                                    if(($y['mimeType'] === 'text/html') && $y['body']) {
                                        $FOUND_BODY = decodeBody($y['body']->data);
                                        break;
                                    }
                                }
                            } else if(($p['mimeType'] === 'text/html') && $p['body']) {
                                $FOUND_BODY = decodeBody($p['body']->data);
                                break;
                            }
                        }
                    }
                    if($FOUND_BODY) {
                        break;
                    }
                }
            }
            // let's save all the images linked to the mail's body:
            if($FOUND_BODY && count($parts) > 1){
                $images_linked = array();
                foreach ($parts  as $part) {
                    if($part['filename']){
                        array_push($images_linked, $part);
                    } else{
                        if($part['parts']) {
                            foreach ($part['parts'] as $p) {
                                if($p['parts'] && count($p['parts']) > 0){
                                    foreach ($p['parts'] as $y) {
                                        if(($y['mimeType'] === 'text/html') && $y['body']) {
                                            array_push($images_linked, $y);
                                        }
                                    }
                                } else if(($p['mimeType'] !== 'text/html') && $p['body']) {
                                    array_push($images_linked, $p);
                                }
                            }
                        }
                    }
                }
                // special case for the wdcid...
                preg_match_all('/wdcid(.*)"/Uims', $FOUND_BODY, $wdmatches);
                if(count($wdmatches)) {
                    $z = 0;
                    foreach($wdmatches[0] as $match) {
                        $z++;
                        if($z > 9){
                            $FOUND_BODY = str_replace($match, 'image0' . $z . '@', $FOUND_BODY);
                        } else {
                            $FOUND_BODY = str_replace($match, 'image00' . $z . '@', $FOUND_BODY);
                        }
                    }
                }
                preg_match_all('/src="cid:(.*)"/Uims', $FOUND_BODY, $matches);
                if(count($matches)) {
                    $search = array();
                    $replace = array();
                    // let's trasnform the CIDs as base64 attachements 
                    foreach($matches[1] as $match) {
                        foreach($images_linked as $img_linked) {
                            foreach($img_linked['headers'] as $img_lnk) {
                                if( $img_lnk['name'] === 'Content-ID' || $img_lnk['name'] === 'Content-Id' || $img_lnk['name'] === 'X-Attachment-Id'){
                                    if ($match === str_replace('>', '', str_replace('<', '', $img_lnk->value)) 
                                            || explode("@", $match)[0] === explode(".", $img_linked->filename)[0]
                                            || explode("@", $match)[0] === $img_linked->filename){
                                        $search = "src=\"cid:$match\"";
                                        $mimetype = $img_linked->mimeType;
                                        $attachment = $gmail->users_messages_attachments->get('me', $mlist->id, $img_linked['body']->attachmentId);
                                        $data64 = strtr($attachment->getData(), array('-' => '+', '_' => '/'));
                                        $replace = "src=\"data:" . $mimetype . ";base64," . $data64 . "\"";
                                        $FOUND_BODY = str_replace($search, $replace, $FOUND_BODY);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // If we didn't find the body in the last parts, 
            // let's loop for the first parts (text-html only)
            if(!$FOUND_BODY) {
                foreach ($parts  as $part) {
                    if($part['body'] && $part['mimeType'] === 'text/html') {
                        $FOUND_BODY = decodeBody($part['body']->data);
                        break;
                    }
                }
            }
            // With no attachment, the payload might be directly in the body, encoded.
            if(!$FOUND_BODY) {
                $FOUND_BODY = decodeBody($body['data']);
            }
            // Last try: if we didn't find the body in the last parts, 
            // let's loop for the first parts (text-plain only)
            if(!$FOUND_BODY) {
                foreach ($parts  as $part) {
                    if($part['body']) {
                        $FOUND_BODY = decodeBody($part['body']->data);
                        break;
                    }
                }
            }
            if(!$FOUND_BODY) {
                $FOUND_BODY = '(No message)';
            }
            // Finally, print the message ID and the body
            print_r($message_id . ": " . $FOUND_BODY);
        }

        if ($list->getNextPageToken() != null) {
            $pageToken = $list->getNextPageToken();
            $list = $gmail->users_messages->listUsersMessages('me', ['pageToken' => $pageToken, 'maxResults' => 1000]);
        } else {
            break;
        }
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
 ?>


 <div class="container col-md-12">
       <div class="row">
           <div class="col-md-12 blue"></div>
           <div class="col-md-12 mb-5 ">
                <h1 class="page-header center"><br>
                        Google Mail
                        <hr />
           </div>
           <div class="col-md-12 mb-5"></div>
           
           <div class="col-md-6 col-ms-12">
           <?php

           if (count($results->getLabels()) == 0) {
            print "No labels found.\n";
          } else {
             ?>
                                <h3 class="ml-2 center">Postfächer </h3><hr/>
                                <div class="table-responsive col-md-12">
                                    <table class="table table-bordered table-hover">
                                    <?php
                                        foreach ($results->getLabels() as $label) {
                                        ?>
                                        <tbody>
                                            <td>
                                                <?php
                                                    echo '<button type="button" class="center btn btn-dark col-md-12">';
                                                    echo $label->getName();
                                                    echo "</button>";
                                                ?>
                                            </td>
                                            <td class="center"><button type="button" class="center btn btn-success">bearbeiten</button></td>
                                            <td class="center"><button type="button" class="center btn btn-danger">löschen</button></td>
                                        </tbody>
                                    <?php
                                }
                            }
                        ?>
                        </table>
                        </div>
           </div>
           <div class="col-md-6 col-xs-12">
                        <h3 class="no-opacity center">Neue E-Mail </h3>
                        <hr />
                <?php
                            $email = new Notification();
                
                            if(isset($_POST['submit']))
                            {
                                $to = $_POST['on'];
                                $subject = $_POST['subject'];
                                $message = $_POST['message'];

                                if ($to == "" || $subject == "" || $message == "")
                                {
                                    echo "<div class='alert alert-danger'>E-Mail konnte nicht versendet werden. Versuche es erneut oder benachrichtige den Administrator. </div>";
                                }else{
                                    echo "<div class='alert alert-success'>E-Mail wurde erfolgreich versendet. </div>";
                                    $email->send_email($to, $subject, $message);
                                }
                            }
                        ?>
                        <div class="">
                           <hr />
                           <div class="">
                                <form action="" method="post" >
                                    <label for="title" class="no-opacity">An: </label>
                                    <input type="text" class="form-control" name="on" placeholder="Empfänger" required>
                                    <hr />
                                    <div class="row">
                                        <div class="col-md-12 no-padding">
                                            <label for="title">Betreff</label>
                                            <input type="text" class="form-control" name="subject" placeholder="Betreff" required>
                                            <hr />
                                        </div>
                                        <div class="col-md-12 no-padding">
                                            <label for="title">Nachricht</label>
                                            <textarea type="text" class="form-control" name="message" id="body" placeholder="Schreibe eine Nachricht.."></textarea>
                                            <hr />
                                        </div>
                                        <?php include "../../../includes/classicedit.php"; ?>
                                    </div>  
                                    <input type="submit" class="form-control btn btn-primary blue" name="submit">
                                </form>
                           </div>
                        </div>
            </div>  
        </div>
</div>
<style>
    .blue{
        background: #004085;
        padding-bottom: 10px;
    }
    .center{
        text-align: center;
    }
    .lightgrey{
        background-color: rgba(0, 0, 0, 0.1);
        border-radius: 5px;
        padding-bottom: 15px;
    }
    .no-opacity{
        opacity: 1;
    }
    .right{
        float: right;
    position: absolute;
    right: 90px;
    }
    </style>
