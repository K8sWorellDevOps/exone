<?php 
    include "../includes/db.php" ;
    include "function.php" ;
    ob_start();
    session_start();

    if(!isset($_SESSION['user_role']))
    {
        header("location: ../index.php");
    }
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8" />
    <xml version="1.0" encoding="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#004085">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Das individuelle Smart Home">
    <meta name="author" content="Tobias Trapp">
    <title>EXONE</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
    <script type="text/javascript" src="js/ckeditor.js.map"></script>
    <script type="text/javascript" src="js/ckeditor.js"></script>
    <script type="text/javascript" src="js/dropzone.js"></script>
    <link href="css/dropzone.css" rel="stylesheet">
</head>
<body>