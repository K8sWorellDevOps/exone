<?php 
    if(isset($_GET['p_id']))
    {
        $get_post_id = $_GET['p_id'];
    }

    $query = "SELECT * FROM posts WHERE id = $get_post_id";
    $select_posts_by_id = mysqli_query($connection, $query);
    
    while ($row = mysqli_fetch_assoc($select_posts_by_id))
    {
        $post_id = $row['id'];
        $post_title = $row['post_title'];
        $post_author = $row['post_author'];
        $post_date = $row['post_date'];
        $post_category_id = $row['post_category_id'];
        $post_status = $row['post_status'];
        $post_image = $row['post_image'];
        $post_tags = $row['post_tags'];
        $post_headline = $row['post_headline'];
        $post_content = $row['post_content'];
        $post_comment_count = $row['post_comment_count'];
    }

    if(isset($_POST['update_post']))
    {
        $post_title = $_POST['post_title'];
        $post_author = $_POST['post_author'];
        $post_category_id = $_POST['post_category'];
        $post_status = $_POST['post_status'];
        $post_image = $_FILES['image']['name'];
        $post_image_temp = $_FILES['image']['tmp_name'];
        $post_tags = $_POST['post_tags'];
        $post_headline = $_POST['post_headline'];
        $post_content = $_POST['post_content'];

        $file = addslashes(file_get_contents($_FILES['image']['tmp_name']));

        // move_uploaded_file($post_image_temp, "../images/$post_image");

        if(empty($post_image))
        {
            $query = "SELECT * FROM posts WHERE id = $get_post_id ";
            $select_image = mysqli_query($connection, $query);
            
            while($row = mysqli_fetch_assoc($select_image))
            {
                $post_image = $row['post_image'];
            }
        }
    
        $query = "UPDATE posts SET ";
        $query .= "post_title = '{$post_title}', ";
        $query .= "post_author = '{$post_author}', ";
        $query .= "post_category_id = '{$post_category_id}', ";
        $query .= "post_status = '{$post_status}', ";
        $query .= "post_image = '{$file}', ";
        $query .= "post_tags = '{$post_tags}', ";
        $query .= "post_headline = '{$post_headline}', ";
        $query .= "post_content = '{$post_content}' ";
        $query .= "WHERE id = {$post_id} ";

        $update_post = mysqli_query($connection, $query);


        echo "<div class='alert alert-success'>Beitrag wurde aktualisiert. " . "" . "<a href='post_all.php'> Alle Beiträge ansehen</a></div>";
        confirm($update_post);
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Post Titel</label>
        <input type="text" class="form-control" name="post_title" value="<?= $post_title;?>">
    </div>  
    <div class="form-group">
        <label for="title">Post Untertitel</label>
        <input type="text" class="form-control" name="post_headline" value="<?= $post_headline;?>">
    </div> 
    <div class="form-group">
        <select name="post_category" id="post_category" class="form-control" style="width: 250px">
            <?php 
                $query = "SELECT * FROM categories";
                $select_value = mysqli_query($connection, $query);
                confirm($select_value);

                while($row = mysqli_fetch_assoc($select_value))
                {
                    $_id = $row['id'];
                    $_title = $row['title'];
                    echo "<option value='$_id'>{$_title}</option>";
                }
            ?>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Post Author</label>
        <input type="text" class="form-control" name="post_author" value="<?= $post_author;?>">
    </div> 
    <div class="form-group">
        <label for="title">Post Status: <?= $post_status;?></label>
        <select name="post_status" id="room_status" class="form-control" style="width: 250px">
            <option value="aktiv">aktiv</option>
            <option value="inaktiv">inaktiv</option>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Post Bild</label><br>
        <?php echo '<img width="250px" src="data:image/jpeg;base64,'.base64_encode($post_image).'">'; ?>
        <input type="file" name="image">
    </div> 
    <div class="form-group">
        <label for="title">Post Tags</label>
        <input type="text" class="form-control" name="post_tags" value="<?= $post_tags;?>">
    </div> 
    <div class="form-group">
        <label for="title">Text Bereich</label>
        <textarea type="text" class="form-control" name="post_content" id="body" height="300"><?= $post_content;?></textarea>
    </div> 
    <?php include "classicedit.php"; ?>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="update_post">
    </div> 
</form>
