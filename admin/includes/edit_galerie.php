<?php 
    if(isset($_GET['p_id']))
    {
        $get_lights_id = $_GET['p_id'];
    }

    $query = "SELECT * FROM galerie WHERE id = $get_lights_id";
    $select_lights_by_id = mysqli_query($connection, $query);
    
    while ($row = mysqli_fetch_assoc($select_lights_by_id))
    {
        $id = $row['id'];
        $comment_image = $row['comment'];
        $image = $row['image'];
    }

    if(isset($_POST['update_image']))
    {
        $comment = $_POST['comment'];
        $category = $_POST['post_category'];
        $image = $_FILES['image']['name'];
        $image_temp = $_FILES['image']['tmp_name'];

        // move_uploaded_file($light_image_temp, "../images/$light_image");
        $file = addslashes(file_get_contents($_FILES['image']['tmp_name']));

        if(empty($image))
        {
            $query = "SELECT * FROM galerie WHERE id = $get_lights_id ";
            $select_image = mysqli_query($connection, $query);
            
            while($row = mysqli_fetch_assoc($select_image))
            {
                $image = $row['image'];
            }
        }
    
        $query = "UPDATE galerie SET ";
        $query .= "comment = '{$comment}', ";
        $query .= "image = '{$file}', ";
        $query .= "category = '{$category}' ";
        $query .= "WHERE id = {$id} ";

        $update_light = mysqli_query($connection, $query);

        echo "<div class='alert alert-success'>Galerie wurde aktualisiert. " . "" . "<a href='galerie.php'> Alle Bilder in der Galerie ansehen</a></div>";
        confirm($update_light);
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Bild auswählen</label><br>
        <?php echo '<img width="250px" src="data:image/jpeg;base64,'.base64_encode($image).'">'; ?>
        <input type="file" name="image" value="<?php $image ?>">
    </div> 
    <hr />
    <div class="form-group col-md-6">
    <label for="title">Kategorie wählen</label>
    <select name="post_category" id="post_category" class="form-control">
            <?php 
                $query = "SELECT * FROM categories";
                $select_value = mysqli_query($connection, $query);

                while($row = mysqli_fetch_assoc($select_value))
                {
                    $_id = $row['id'];
                    $_title = $row['title'];
                    echo "<option value='$_title'>{$_title}</option>";
                }
            ?>
        </select>
</div>
    <div class="form-group col-md-6">
        <label for="title">Beitragstext</label>
        <textarea type="text" class="form-control" name="comment" id="body"><?= $comment_image; ?></textarea>
    </div> 
        <?php include "classicedit.php"; ?>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="update_image">
    </div> 
</form>
