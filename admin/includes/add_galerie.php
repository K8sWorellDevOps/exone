<link href="css/bootstrap.min.css" rel="stylesheet" >

<?php

    require_once('../classes/Notification.php');

    $mail = new Notification();

    if(isset($_FILES['file']))
    {
        $cat = $_POST['post_category'];
        $comment = $_POST['comment'];
        $image = $_FILES['file']['name'];
        $post_image_temp = $_FILES['file']['tmp_name'];

        $mail->send_email($mail->getEmail(), "Galerie Update", "Ein neues Bild wurde der Galerie hinzugefügt.");
    }

    if($_FILES['file'] != "")
    {
        $file_ = addslashes(file_get_contents($_FILES['file']['tmp_name']));
        // move_uploaded_file($post_image_temp, "../images/$post_image");
        $query = "INSERT INTO galerie(image, category, comment)";
        $query .= "VALUES('{$file_}','{$cat}','{$comment}')";
        $create_post_query = mysqli_query($connection, $query);
        confirm($create_post_query);
    }

    echo "<div class='alert alert-info'>" . "" . "<a href='galerie.php'> Alle Bilder in der Galerie ansehen</a></div>";
?>

<div class="row">
    <div class="col-md-12">
        <h2>Drag and Drop</h2>
        <h3>Mehrere Bilder hochladen - ohne Text</h1>
        <hr />
        <form action="galerie.php?source=add_galerie" class="dropzone">

        </form>
    </div>
</div>
<hr />

<div class="row col-md-12">
    <h3>1 Bild hochladen mit Text</h1>
    <hr />
<form action="" method="post" class="col-md-12" enctype="multipart/form-data">
<div class="form-group col-md-6">
    <label for="title">Bild</label>
    <input type="file" class="form-control" name="file">
</div>
<div class="form-group col-md-6">
    <label for="title">Kategorie wählen</label>
    <select name="post_category" id="post_category" class="form-control">
            <?php 
                $query = "SELECT * FROM categories";
                $select_value = mysqli_query($connection, $query);

                while($row = mysqli_fetch_assoc($select_value))
                {
                    $_id = $row['id'];
                    $_title = $row['title'];
                    echo "<option value='$_title'>{$_title}</option>";
                }
            ?>
        </select>
</div>
<div class="form-group">
    <label for="title">Beitragstext</label>
    <textarea type="text" class="form-control" name="comment" id="body"></textarea>
</div>
<?php include "classicedit.php"; ?>
<div class="form-group">
    <input type="submit" class="btn btn-success" name="submit">
</div> 
</form>
</div>