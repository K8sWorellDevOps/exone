<link href="css/bootstrap.min.css" rel="stylesheet" >
<div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Raumname</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">ChipID</th>
                                    <th scope="col">Stromverbrauch</th>
                                    <th scope="col">Steckdosen</th>
                                    <th scope="col">Fenster</th>
                                    <th scope="col">Waschmaschine</th>
                                    <th scope="col">Wasserverbrauch</th>
                                    <th scope="col">Kamera</th>
                                    <th scope="col">Bild</th>
                                    <th scope="col">bearbeiten</th>
                                    <th scope="col">löschen</th>
                                </tr>
                            </thead>
                            <tbody>                      
                            <?php 
                                $query = "SELECT * FROM rooms";
                                $select_all_rooms = mysqli_query($connection, $query);
                                
                                while ($row = mysqli_fetch_assoc($select_all_rooms))
                                {
                                    $room_id = $row['room_id'];
                                    $room_name = $row['room_name'];
                                    $room_status = $row['room_status'];
                                    $room_chipID = $row['chip_room_ID'];
                                    $room_energy = $row['room_energy'];
                                    $room_sockets = $row['room_sockets'];
                                    $room_windows = $row['room_windows'];
                                    $room_washing_machine = $row['room_washing_machine'];
                                    $room_water = $row['room_water'];
                                    $room_camera = $row['room_camera'];
                                    $room_image = $row['room_image'];
                                
                                    echo "<tr>";
                                    echo "<th scope='row'>{$room_id}</th>";
                                    echo "<td>{$room_name}</td>";
                                    echo "<td>{$room_status}</td>";
                                    echo "<td>{$room_chipID}</td>";
                                // $query = "SELECT * FROM categories WHERE id = {$post_category_id}";
                                //     $select_categories = mysqli_query($connection, $query);
                                    
                                //     while ($row = mysqli_fetch_assoc($select_categories))
                                //     {
                                //         $id = $row['id'];
                                //         $title = $row['title'];
                                //           echo "<td>{$title}</td>";
                                //     }
                                    echo "<td>{$room_energy}</td>";
                                    echo "<td>{$room_sockets}</td>";
                                    echo "<td>{$room_windows}</td>";
                                    echo "<td>{$room_washing_machine}</td>";
                                    echo "<td>{$room_water}</td>";
                                    echo "<td>{$room_camera}</td>";
                                    echo '<td><img width="150px" src="data:image/jpeg;base64,'.base64_encode($room_image).'"></td>';
                                    echo "<td><a href='rooms.php?source=edit_rooms&p_id={$room_id}'>bearbeiten</a></td>";
                                    echo "<td><a href='rooms.php?delete={$room_id}'>löschen</a></td>";
                                    echo "</tr>";
                                }
                            
                            if (isset($_GET['delete']))
                            {
                                $delete_room_id = $_GET['delete'];

                                $query = "DELETE FROM rooms WHERE id = {$delete_room_id}";
                                $delete_query = mysqli_query($connection, $query);
                                header("Location: rooms.php");
                            }
                            
                            ?>
                          