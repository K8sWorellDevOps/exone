<?php 
    if(isset($_POST['create_user']))
    {
        $user_id = $_POST['id'];
        $user_name = $_POST['name'];
        $geburtstag = $_POST['bday'];
        $anschrift = $_POST['home'];
        $tel = $_POST['tel'];
        // $post_image = $_FILES['post_image']['name'];
        // $post_image_temp = $_FILES['post_image']['tmp_name'];
        $user_role = $_POST['user_role'];
        $user_email = $_POST['email'];
        $user_password = $_POST['password'];
        // $post_date = date('d-m-y');
        // $post_comment_count = 4;

        // move_uploaded_file($post_image_temp, "../images/$post_image");

        $query = "INSERT INTO users(id, name, bday, home, tel, user_role, email,password)";
        $query .= "VALUES('{$user_id}','{$user_name}','{$geburtstag}','{$anschrift}','{$tel}','{$user_role}','{$user_email}', '{$user_password}')";
        $create_user_query = mysqli_query($connection, $query);

        echo "<div class='alert alert-success'>Benutzer wurde erstellt. " . "" . "<a href='users.php'> Alle Benutzer ansehen</a></div>";
        confirm($create_user_query);
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Name</label>
        <input type="text" class="form-control" name="name">
    </div>  
    <label>Role</label>
    <div class="form-group">
        <select name="user_role" id="user_role" class="form-control" style="width: 250px">
          <option value="subscriber">Wähle eine Option</option>
          <option value="admin">Admin</option>
          <option value="subscriber">Teilnehmer</option>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Geburtstag</label>
        <input type="date" class="form-control" name="bday">
    </div> 
    <div class="form-group">
        <label for="title">Anschrift</label>
        <input type="text" class="form-control" name="home">
    </div> 
    <div class="form-group">
        <label for="title">Telefonnummer</label>
        <input type="tel" class="form-control" name="tel">
    </div> 
    <div class="form-group">
        <label for="title">Email</label>
        <input type="email" class="form-control" name="email">
    </div> 
    <div class="form-group">
        <label for="title">Passwort</label>
        <input type="password" class="form-control" name="password">
    </div> 
    <!-- <div class="form-group">
        <label for="title">Image</label>
        <input type="file" class="form-control" name="user_image">
    </div>  -->
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_user" value="Benutzer anlegen">
    </div> 
</form>