<link href="css/bootstrap.min.css" rel="stylesheet" >
<?php 

    require_once('../classes/Notification.php');
  
    $mail = new Notification();
    
    if(isset($_POST['create_room']))
    {
        $room_name = $_POST['room_name'];
        $room_status = $_POST['room_status'];
        $room_temp = $_POST['room_temp'];
        $room_air = $_POST['room_air'];
        $room_image = $_FILES['room_image']['name'];
        $room_image_temp = $_FILES['room_image']['tmp_name'];
        $room_energy = $_POST['room_energy'];
        $room_sockets = $_POST['room_sockets'];
        $room_windows = $_POST['room_windows'];
        $room_washing_machine = $_POST['room_washing_machine'];
        $room_water = $_POST['room_water'];
        $room_camera = $_POST['room_camera'];
        // $post_date = date('d-m-y');
        // $post_comment_count = 4;
        $file = addslashes(file_get_contents($_FILES['room_image']['tmp_name']));
        // move_uploaded_file($room_image_temp, "../images/$room_image");

        $query = "INSERT INTO rooms(room_name, room_status, room_temp, room_air, room_energy, room_sockets, room_windows,room_washing_machine,room_water, room_camera, room_image)";
        $query .= "VALUES('{$room_name}','{$room_status}','{$room_temp}','{$room_air}','{$room_energy}','{$room_sockets}','{$room_windows}','{$room_washing_machine}', '{$room_water}', '{$room_camera}','{$room_image}')";
        $create_room_query = mysqli_query($connection, $query);

        confirm($create_room_query);

        $mail->send_email("tobiastrapp23@gmail.com", "Raum Update", "Ein neuer Raum wurde hinzugefügt.");
    }
?>
<form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Raumname</label>
        <input type="text" class="form-control" name="room_name">
    </div>  
    <div class="form-group">
        <label for="title">Status</label>
        <select name="room_status" id="room_status" class="form-control" style="width: 250px">
            <option value="aktiv">aktiv</option>
            <option value="inaktiv">inaktiv</option>
        </select>
    </div>
    <div class="form-group">
        <label for="title">Status der Temperatur</label>
        <input type="text" class="form-control" name="room_temp">
    </div> 
    <div class="form-group">
        <label for="title">Status der Luftfeuchtigkeit</label>
        <input type="text" class="form-control" name="room_air">
    </div> 
    <div class="form-group">
        <label for="title">Status des Stromverbrauches</label>
        <input type="text" class="form-control" name="room_energy">
    </div> 
    <div class="form-group">
        <label for="title">Bild</label>
        <input type="file" class="form-control" name="room_image">
    </div> 
    <div class="form-group">
        <label for="title">Status der Steckdosen</label>
        <input type="text" class="form-control" name="room_sockets">
    </div> 
    <div class="form-group">
        <label for="title">Status der Fenster</label>
        <textarea type="text" class="form-control" name="room_windows" height="300"></textarea>
    </div> 
    <div class="form-group">
        <label for="title">Status der Waschmaschine</label>
        <textarea type="text" class="form-control" name="room_washing_machine" height="300"></textarea>
    </div> 
    <div class="form-group">
        <label for="title">Status des Wasserverbrauchs</label>
        <textarea type="text" class="form-control" name="room_water" height="300"></textarea>
    </div>
    <div class="form-group">
        <label for="title">Status der Kameras</label>
        <textarea type="text" class="form-control" name="room_camera" height="300"></textarea>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="create_room">
    </div> 
</form>