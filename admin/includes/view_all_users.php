        <link href="css/bootstrap.min.css" rel="stylesheet" >
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" class="alert alert-danger">ID</th>
                                    <th scope="col" class="alert alert-info">Profilbild</td>
                                    <th scope="col" class="alert alert-info">Name</th>
                                    <th scope="col" class="alert alert-info">Geburtstag</th>
                                    <th scope="col" class="alert alert-info">Anschrift</th>
                                    <th scope="col" class="alert alert-info">Telefonnummer</th>
                                    <th scope="col" class="alert alert-info">Email</th>
                                    <th scope="col" class="alert alert-info">Role</th>
                                    <th scope="col" class="alert alert-info">Admin role</th>
                                    <th scope="col" class="alert alert-info">Teilnehmer role</th>
                                    <th scope="col" class="alert alert-warning">bearbeiten</th>
                                    <th scope="col" class="alert alert-warning">löschen</th>
                                </tr>
                            </thead>
                            <tbody>

                            <?php 
                                $query = "SELECT * FROM users";
                                $select_all_users = mysqli_query($connection, $query);
                                
                                while ($row = mysqli_fetch_assoc($select_all_users))
                                {
                                    $user_id = $row['id'];
                                    $user_name = $row['name'];
                                    $user_bday = $row['bday'];
                                    $user_home = $row['home'];
                                    $user_tel = $row['tel'];
                                    $user_email = $row['email'];
                                    $image_path = $row['image_path'];
                                    $user_image = $row['user_image'];
                                    $user_role= $row['user_role'];
                                    $usre_date = $row['created_at'];
                                
                                    echo "<tr>";
                                    echo "<th scope='row' class='alert alert-danger'>{$user_id}</th>";
                                    echo '<td class="alert alert-info"><img style="width: 100%" src="data:image/jpeg;base64,'.base64_encode($row['user_image']).'"></td>';
                                    echo "<td class='alert alert-info'>{$user_name}</td>";

                                
                                    // $query = "SELECT * FROM categories WHERE id = {$post_category_id}";
                                    // $select_categories = mysqli_query($connection, $query);
                                    
                                    // while ($row = mysqli_fetch_assoc($select_categories))
                                    // {
                                    //     $id = $row['id'];
                                    //     $title = $row['title'];
                                    //     echo "<td>{$title}</td>";
                                    // }

                                    echo "<td class='alert alert-info'>{$user_bday}</td>";
                                    echo "<td class='alert alert-info'>{$user_home}</td>";
                                    echo "<td class='alert alert-info'>{$user_tel}</td>";

                                    // $query = "SELECT * FROM posts WHERE id = $comment_post_id ";
                                    // $select_post_id_query = mysqli_query($connection, $query);
                                    // while($row = mysqli_fetch_assoc($select_post_id_query))
                                    // {
                                    //     $post_id = $row['id'];
                                    //     $post_title = $row['post_title'];
                                    //     echo "<td><a href='../post.php?p_id=$post_id'>$post_title</a></td>";
                                    // }

                                    echo "<td class='alert alert-info'>{$user_email}</td>";
                                    echo "<td class='alert alert-info'>{$user_role}</td>";
                                    echo "<td class='alert alert-info'><a href='users.php?change_to_admin={$user_id}'>Admin</a></td>";
                                    echo "<td class='alert alert-info'><a href='users.php?change_to_subscriber={$user_id}'>Subscriber</a></td>";
                                    echo "<td class='alert alert-warning'><a href='users.php?source=edit_user&edit_user={$user_id}'>bearbeiten</a></td>";
                                    echo "<td class='alert alert-warning'><a href='users.php?delete={$user_id}'>löschen</a></td>";
                                    echo "</tr>";
                                }
                                ?>  
                                </tbody>
                            </table>
                            <?php
                                if (isset($_GET['change_to_admin']))
                                {
                                    $user_id = $_GET['change_to_admin'];

                                    $query = "UPDATE users SET user_role = 'admin' WHERE id = $user_id";
                                    $admin_query = mysqli_query($connection, $query);
                                    header("Location: users.php");
                                }

                                if (isset($_GET['change_to_subscriber']))
                                {
                                    $user_id = $_GET['change_to_subscriber'];

                                    $query = "UPDATE users SET user_role = 'subscriber' WHERE id = $user_id";
                                    $subscriber_query = mysqli_query($connection, $query);
                                    header("Location: users.php");
                                }
                            
                                if (isset($_GET['delete']))
                                {
                                    $comment_id = $_GET['delete'];

                                    $query = "DELETE FROM users WHERE id = {$user_id}";
                                    $delete_user_query = mysqli_query($connection, $query);
                                    header("Location: users.php");
                                }
                                
                            ?>