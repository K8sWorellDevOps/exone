
<?php 

    include("photo_library_modal.php");

    if(isset($_GET['edit_user']))
    {
        $the_user_id = $_GET['edit_user'];

        $query = "SELECT * FROM users WHERE id = $the_user_id ";
        $select_users = mysqli_query($connection, $query);
                                
        while ($row = mysqli_fetch_assoc($select_users))
        {
            $user_id = $row['id'];
            $user_name = $row['name'];
            $user_bday = $row['bday'];
            $user_home = $row['home'];
            $user_tel = $row['tel'];
            $user_email = $row['email'];
            $user_role= $row['user_role'];
            $user_password = $row['password'];
            $user_image = $row['user_image'];
        }
    }

    if(isset($_POST['edit_user']))
    {
        $user_id = $_POST['id'];
        $user_name = $_POST['name'];
        $user_bday = $_POST['bday'];
        $user_home = $_POST['home'];
        $user_tel = $_POST['tel'];
        $post_image = $_FILES['image']['name'];
        $post_image_temp = $_FILES['image']['tmp_name'];
        $user_role = $_POST['user_role'];
        $user_email = $_POST['email'];
        $user_password = $_POST['password'];
        // $post_date = date('d-m-y');
        // $post_comment_count = 4;

        // move_uploaded_file($post_image_temp, "../images/$post_image");


        $blob = addslashes(file_get_contents($_FILES['image']['tmp_name']));

        
        $query = "SELECT * FROM users";
        $select_randsalt_query = mysqli_query($connection, $query);

        if(!$select_randsalt_query)
        {
            die("Fehlgeschlagen 2" . mysqli_error($connection));
        }

        $row = mysqli_fetch_array($select_randsalt_query);
        $salt = $row['randSalt'];
        $hashed_passwort = crypt($user_password, $salt);
    
        $query = "UPDATE users SET ";
        $query .= "name = '{$user_name}', ";
        $query .= "bday = '{$user_bday}', ";
        $query .= "home = '{$user_home}', ";
        $query .= "tel = '{$user_tel}', ";
        $query .= "email = '{$user_email}', ";
        $query .= "user_role = '{$user_role}', ";
        $query .= "password = '{$hashed_passwort}', ";
        $query .= "user_image = '{$blob}' ";
        $query .= "WHERE id = {$the_user_id} ";

        $update_user = mysqli_query($connection, $query);

        if(!$update_user)
        {
            die("Fehlgeschlagen 2" . mysqli_error($connection));
        }else{
            echo "<div class='alert alert-success'>Benutzer wurde aktualisiert. " . "" . "<a href='users.php'> Alle Benutzer ansehen</a></div>";
            confirm($update_user);
        }
    }
?>



<form action="" method="post" enctype="multipart/form-data">
<a href="#" data-toggle="modal" data-target="#photo-library">
    <?php 
        echo '<img style="width: 30%; height: auto; float: left; margin: auto;" src="data:image/jpeg;base64,'.base64_encode($user_image).'">'; 
    ?>
    </a>
    <div class="form-group">
        <label for="title" style="float: left;">Profilbild</label>
        <input type="file" class="form-control" name="image">
    </div> 
    <div class="form-group">
        <label for="title">Name</label>
        <input type="text" class="form-control" name="name" value="<?= $user_name; ?>">
    </div>  
    <label>Role</label>
    <div class="form-group">
        <select name="user_role" id="user_role">
            <option value="subscriber"><?= $user_role; ?></option>
            <?php
                if($user_role == 'admin')
                {
                    echo "<option value='subscriber'>teilnehmer</option>";
                }else{
                    echo " <option value='admin'>admin</option>";
                }
            ?>
        </select>
    </div> 
    <div class="form-group">
        <label for="title">Geburtstag</label>
        <input type="date" class="form-control" name="bday" value="<?= $user_bday; ?>">
    </div> 
    <div class="form-group">
        <label for="title">Anschrift</label>
        <input type="text" class="form-control" name="home" value="<?= $user_home; ?>">
    </div> 
    <div class="form-group">
        <label for="title">Telefonnummer</label>
        <input type="tel" class="form-control" name="tel" value="<?= $user_tel; ?>">
    </div> 
    <div class="form-group">
        <label for="title">Email</label>
        <input type="email" class="form-control" name="email" value="<?= $user_email; ?>">
    </div> 
    <div class="form-group">
        <label for="title">Passwort</label>
        <input type="password" class="form-control" name="password" value="<?= $user_password; ?>">
    </div> 
    <div class="form-group">
        <input type="submit" class="btn btn-primary" name="edit_user" value="Benutzer aktualisieren">
    </div> 
</form>
