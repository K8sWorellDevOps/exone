


<?php 
    include "includes/admin_header.php";

    if(isset($_SESSION['email']))
    {
        $email = $_SESSION['email'];
        $query = "SELECT * FROM users WHERE email = '{$email}' ";
        $select_user_profile_query = mysqli_query($connection, $query);

        while($row = mysqli_fetch_array($select_user_profile_query))
        {
            $user_id = $row['id'];
            $user_name = $row['name'];
            $user_bday = $row['bday'];
            $user_home = $row['home'];
            $user_tel = $row['tel'];
            $user_email = $row['email'];
            $user_pass = $row['password'];
            $user_role= $row['user_role'];
            $user_image= $row['user_image'];
        }
    }

    if(isset($_POST['edit_user']))
    {
        $user_id = $_POST['id'];
        $user_name = $_POST['name'];
        $user_bday = $_POST['bday'];
        $user_home = $_POST['home'];
        $user_tel = $_POST['tel'];
        // $post_image = $_FILES['post_image']['name'];
        // $post_image_temp = $_FILES['post_image']['tmp_name'];
        $user_role = $_POST['user_role'];
        $user_email = $_POST['email'];
        $user_password = $_POST['password'];
        // $post_date = date('d-m-y');
        // $post_comment_count = 4;

        // move_uploaded_file($post_image_temp, "../images/$post_image");
    
        $query = "UPDATE users SET ";
        $query .= "name = '{$user_name}', ";
        $query .= "bday = '{$user_bday}', ";
        $query .= "home = '{$user_home}', ";
        $query .= "tel = '{$user_tel}', ";
        $query .= "email = '{$user_email}', ";
        $query .= "user_role = '{$user_role}', ";
        $query .= "password = '{$user_password}' ";
        $query .= "WHERE id = {$the_user_id} ";

        $update_user = mysqli_query($connection, $query);
        confirm($update_user);
    }
?>
<link href="css/bootstrap.min.css" rel="stylesheet" >
<div id="wrapper" class="wrapper">
    <?php include "includes/admin_nav.php"; ?>
    <div id="page-wrapper">
        <div class="containter-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><br><br><br>
                        Profil
                        <small> <br>Übersicht von: <b><?= $user_name; ?></b></small>
                    </h1>

                   <form action="" method="post" enctype="multipart/form-data">
                   <?php echo '<td><img width="150px" src="data:image/jpeg;base64,'.base64_encode($user_image).'">'; ?>

                        <div class="form-group">
                            <label for="title">Name</label>
                            <input type="text" class="form-control" name="name" value="<?= $user_name; ?>">
                        </div>  
                        <label>Role</label>
                        <div class="form-group">
                            <select name="user_role" id="user_role">
                                <option value="subscriber"><?= $user_role; ?></option>
                                <?php
                                    if($user_role == 'admin')
                                    {
                                        echo "<option value='subscriber'>teilnehmer</option>";
                                    }else{
                                        echo " <option value='admin'>admin</option>";
                                    }
                                ?>
                            </select>
                        </div> 
                        <div class="form-group">
                            <label for="title">Geburtstag</label>
                            <input type="date" class="form-control" name="bday" value="<?= $user_bday; ?>">
                        </div> 
                        <div class="form-group">
                            <label for="title">Anschrift</label>
                            <input type="text" class="form-control" name="home" value="<?= $user_home; ?>">
                        </div> 
                        <div class="form-group">
                            <label for="title">Telefonnummer</label>
                            <input type="tel" class="form-control" name="tel" value="<?= $user_tel; ?>">
                        </div> 
                        <div class="form-group">
                            <label for="title">Email</label>
                            <input type="email" class="form-control" name="email" value="<?= $user_email; ?>">
                        </div> 
                        <div class="form-group">
                            <label for="title">Passwort</label>
                            <input type="password" class="form-control" name="password" value="<?= $user_pass; ?>">
                        </div> 
                        <!-- <div class="form-group">
                            <label for="title">Image</label>
                            <input type="file" class="form-control" name="user_image">
                        </div>  -->
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="edit_user" value="Profil aktualisieren">
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "includes/admin_footer.php"; ?>