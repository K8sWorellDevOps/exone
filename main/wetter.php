<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>EXONE - Das Smart Home</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/c\ss/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
    <script type="text/javascript"     src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script src="classes/commons/jquery/jquery-1.7.1.min.js"><\/script>')</script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script> 
    <link rel="stylesheet" type="text/css" media="screen" href="/exone/assets/css/main.css" />
    <link rel="stylesheet"  media="screen" href="../../../exone/assets/css/switch.css" />
</head>
<?php
    session_start();
    
    include("../../exone/header.php"); 
    include("../../exone/nav.php");

    if(!isset($_SESSION['userid'])) 
    {
        header("location: localhost://index.php");
    }
    
    $userid = $_SESSION['userid'];
?>
<div class="mt-5 pt-3">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-white">
            <li class="breadcrumb-item active" aria-current="page">Home</li>
           <li class="breadcrumb-item" aria-current="page">Wetter</li>
        </ol>
    </nav>
    <br>
</div>
  
  <div class="container">
        <center>
            <h2>Temperatur</h2>
        </center>
        <hr/>
        <div class="row">
            <div class="col-md-6 box bg-white" id='shot' style="float: left;">
                <form>
                    <div class="form-group">
                        <div class="alert alert-info" role="alert">Innentemperatur: <?php include "shell/arduino/sensordata.php"; ?> C°</div>
                        <hr />
                    </div>
                </form>
            </div>
            <div class="col-md-6 box bg-white" id='shot'>
                <form>
                    <div class="form-group">
                        <div class="alert alert-info" role="alert">Außentemperatur: C°</div>
                        <hr />
                    </div>
                </form>
            </div>
        </div>
        <img src="/exone/main/shell/arduino/temp.png" width="100%"> 
</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/jquery-slim.min.js"><\/script>')</script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script>
        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').trigger('focus')
        })
    </script>
  </body>
<?php
    include("../footer.php");
?>