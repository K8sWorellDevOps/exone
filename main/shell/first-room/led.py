#!/usr/bin/env python
#coding=utf-8

import time
import RPi.GPIO as GPIO

GPIO.setwarnings(False)

# Z  hlweise der Pins festlegen
GPIO.setmode(GPIO.BOARD)


# Pin 22 (GPIO 25) als Ausgang festlegen
GPIO.setup(40, GPIO.OUT)
GPIO.output(40, GPIO.HIGH)