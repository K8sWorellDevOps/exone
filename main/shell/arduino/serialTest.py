#!/usr/bin/python
import serial
import MySQLdb
import os
import sys
import time
import csv

s = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
s.isOpen()
print("\nDaten werden abgerufen...\n")
time.sleep(10)

f = open("sensordata.php", 'w')

message = """<html>
<head><meta charset="utf-8" />
<meta http-equiv="refresh" content="30" />
  <div class="" style="float:right;">
		%s		
    </div>
</html>"""

dbServer='192.168.2.106'
dbPass='2uItjDDsvV1CvCrI'
dbSchema='exone'
dbUser='exone'

s.write("test")
try:
    while True:

        #
        #
        #
        # save temperature into database
        #
        #
        #

        response = s.readline().splitlines()
        print(response)
        whole = message % (response)
        f.flush()
        f.write(whole)

        connection = MySQLdb.connect(host=dbServer,user=dbUser,passwd=dbPass,db=dbSchema)

        value = response

        cursor = connection.cursor()
        cursor.execute("INSERT INTO weather (inside) VALUES (%s)", (value))
        connection.commit()
        cursor.close();
        print(value)
        print("\nDaten wurden erfolgreich abgespeichert\n")

        #
        #
        #
        # update CSV data from database -> temp.csv
        #
        #
        #

        print("\nCSV Datei wird aktualisiert...\n")

        dbQuery='SELECT * FROM weather;'

        db=MySQLdb.connect(host=dbServer,user=dbUser,passwd=dbPass,db=dbSchema)
        cur=db.cursor()
        cur.execute(dbQuery)
        result=cur.fetchall()

        c = csv.writer(open("temp.csv", 'w'))
        for row in result:
            print( str(row).replace('),', '),\r\n') )
            c.writerow(row)

        print("\nCSV Datei wurde erfolgreich aktualisiert.\n")
        sys.exit(0)

except KeyboardInterrupt:
  print("\nFehler beim Abruf oder dem speichern der Daten!\n")
  s.close()