#!/usr/bin/env python3
#coding=utf-8
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import csv
import time

x=[]
y=[]

with open('/Applications/MAMP/htdocs/exone/main/shell/arduino/temp.csv', 'r') as csvfile:
    plots= csv.reader(csvfile, delimiter=',')
    for row in plots:
        x.append(time.strftime(row[2]))
        y.append(float(row[1]))

plt.figure(figsize=(20,6))
plt.subplots_adjust(bottom=0.3)
plt.plot(x,y, marker='o', label='Verlauf')
plt.xticks(x, rotation='vertical')
plt.grid(True)
plt.legend(loc='right')
plt.title("Temperaturverlauf")
plt.ylabel("In C°")
plt.xlabel("Datum")
plt.show()