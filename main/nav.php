<?php include "../../admin/function.php"; ?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#004085">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Original - Lifestyle Blog Template</title>
    <link rel="icon" href="img/core-img/favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../style.css">
</head>

<body>
    <header class="header-area">
        <div class="logo-area text-center">
            <div class="container h-100">
                <div class="row h-100 align-items-center">
                    <div class="col-12">
                        <a href="index.php" class="original-logo black"><img src="" alt=""><h1 class="black" style="color: black;">EXONE</h1></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="original-nav-area" id="stickyNav">
        <div class="col-md-12 blue"></div>
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between">

                        <!-- Subscribe btn -->
                        <?php if(isLoggedIn()): 
                            $admin =  "<li><a href='admin'>Admin</a></li>";
                        ?>
                        
                            <a href='includes/logout.php' class='btn original-btn'>
                                <p class='mt-3'>ausloggen</p>
                            </a>

                        <?php else: ?>

                            <a href='../../includes/login.php' class='btn original-btn'>
                                <p class="mt-3">Login</p>
                            </a>
                                
                            </a>
                        <?php endif; ?>
                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu" id="originalNav">
                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="/main/dashboard/">Smart Home</a>
                                        <ul class="dropdown">
                                            <!--<li><a href="registration.php">Registrierung</a></li>-->
                                            <li><a href="main/dashboard/">Dashboard</a></li>
                                            <li><a href="galerie.php">Galerie</a></li>
                                            <li><a href="#">Musik</a>
                                            <ul class="dropdown">
                                                <?php if(isLoggedIn()): ?>
                                                    <li><a href="./admin/includes/spotify/">Spotify</a></li>
                                                    <li><a href="./admin/includes/google_music">Google music</a></li>
                                                <?php endif; ?>
                                                </ul>
                                            </li>
                                            <li><a href="lights">Google</a>
                                                <ul class="dropdown">
                                                <?php if(isLoggedIn()): ?>
                                                    <li><a href="./admin/includes/google/calendar/">Kalender</a></li>
                                                    <li><a href="./admin/includes/google/gmail/">Gmail</a></li>
                                                <?php endif; ?>
                                                    <li><a href="#">Entertainment</a>
                                                        <ul class="dropdown">
                                                            <li><a href="./admin/includes/google_music">Google music</a></li>
                                                            <li><a href="#">Catagory 3</a></li>
                                                            <li><a href="#">Catagory 3</a></li>
                                                            <li><a href="#">Catagory 3</a></li>
                                                            <li><a href="#">Catagory 3</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Catagory 2</a></li>
                                                    <li><a href="#">Catagory 2</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Kategorien</a>
                                        <ul class="dropdown">
                                            <?php 
                                                $query = "SELECT * FROM categories";
                                                $select_all = mysqli_query($connection, $query);
                                                while ($row = mysqli_fetch_assoc($select_all))
                                                {
                                                    $title = $row['title'];
                                                    echo "<li><a href='#'>{$title}</a></li>";
                                                }
                                            ?>
                                            <li>
                                                <a href="admin">Admin</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="contact.php">Kontakt</a></li>
                                    <?= $admin; ?>
                                </ul>

                                <?php 
                                    if(ifItIsMethod('post'))
                                    {
                                        if(isset($_POST['login']))
                                        {
                                            if(isset($_POST['username']) && isset($_POST['password']))
                                            {
                                                login_user($_POST['username'], $_POST['password']);
                                            }else{
                                                redirect("index");
                                            }
                                        }
                                    }
                                    
                                ?>

                                <!-- Search Form  -->
                                <div id="search-wrapper">
                                    <form action="search.php" method="post">
                                        <input type="text" id="search"  name="search" placeholder="Suche eingeben...">
                                        <div id="close-icon"></div>
                                        <input class="d-none" type="submit" name="submit">
                                    </form>
                                </div>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <style>
    .blue{
        background: #004085;
        padding-bottom: 7px;
    }
    </style>