
<?php include '../header.php'; ?>
<?php include '../nav.php'; ?>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><br> Willkommen zurück,  <small> <?= $_SESSION['name']; ?></small></h1>
            <ol class="breadcrumb">
                <li>
                    <i class="far fa-lightbulb black i"></i>  <a href="index.php">Lightning</a>
                </li>
            </ol>
        </div>

        <?php #if(isLoggedIn()): ?> 

        <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-5" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert grey-background" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <div class="black">
                                    <i class="fa fa-tv black i"></i>
                                    <h4>Kino</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                        </div>
                        <div class="col-md-2 mx-auto alert lightblue-background" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-bed black i"></i>
                                <h4>Wake up</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                    </div>
                    <div class="col-md-2 mx-auto alert brown-background" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-sun black i"></i>
                                <h4>Dämmerung</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                    </div>
                    <div class="col-md-2 mx-auto alert party-mix grey" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="grey width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="grey fas fa-music black i"></i>
                                    <h4 class="grey">Party</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                        </div>
                        <div class="col-md-2 mx-auto alert alert-primary" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-couch black i"></i>
                                <h4>Gemütlich</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                    </div>
                <div>
            </div>

            <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-3" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert bisque-background" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <div class="black">
                                    <i class="fa fa-snowflake black i"></i>
                                    <h4>Sinnlich</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                        </div>
                        <div class="col-md-2 mx-auto alert gold-background" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-campground black i"></i>
                                <h4>Urlaub</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                    </div>
                    <div class="col-md-2 mx-auto alert royal-background" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-home black i"></i>
                                <h4>Außer Haus</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                    </div>
                    <div class="col-md-2 mx-auto alert lightgray-background" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-trash black i"></i>
                                    <h4>Keller</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                             </div>
                        </div>
                        <div class="col-md-2 mx-auto alert yellowgreen-background" role="alert">
                            <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                            </form>
                            <div class="black">
                                <i class="fas fa-couch black i"></i>
                                <h4>ECO</h4>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" id="heading"></div>
                                <div class="panel-body" id="body"></div>
                            </div>
                    </div>
                <div>
            </div>

            <div class="container col-md-12">
                <div class="box shadow-sm black text-center mt-3" id='shot'>
                    <div class="row">
                        <div class="col-md-2 mx-auto alert alert-danger" role="alert">
                                <form id="myForm2" name="myForm2" action="shell/first-room/first-light.php" method="post"> 
                                    <input type="checkbox" name="toggle" id="toggle" data-toggle="toggle" data-off="OFF" data-on="ON" class="width-100 alert alert-primary">
                                </form>
                                <div class="black">
                                    <i class="fa fa-tree black i"></i>
                                    <h4>Christmas</h4>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" id="heading"></div>
                                    <div class="panel-body" id="body"></div>
                                </div>
                        </div>
                <div>
    </div>

    <?php #else: ?> 

        <!-- <div class="alert alert-danger"><i class="fa fa-warning"></i> Sie nicht nicht berechtigt diese Seite zu sehen! <i class="fa fa-warning"></i></div> -->

    <?php #endif; ?> 
