<div class="col-md-4">
    <div class="well">
        <h4>Blog Search</h4>
        <form action="search.php" method="post">
            <div class="input-group">
                <input type="text" name="search" class="form-control">
                <span class="input-group-btn">
                    <button name="submit" class="btn btn-primary" type="submit">
                        <span class="glyphicon glyphicon-search"></span>
                </button>
                </span>
            </div>
        </form>
    </div>
    <div class="well">
        <h4>Login</h4>
        <hr />
        <form action="includes/login.php" method="post">
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="input-group">
                <input type="password" name="password" class="form-control" placeholder="*******">
                <span class="input-group-btn">
                    <button class="btn btn-primary" name="login" type="submit">
                        <span>
                            einloggen
                        </span>
                    </button>
                </span>
            </div>
        </form>
    <div class="well">
    <?php 
        $query = "SELECT * FROM categories LIMIT 4";
        $select_sidebar = mysqli_query($connection, $query);
    ?>
    <h4>Blog Categories</h4>
    <div class="row">
        <div class="col-lg-12">
            <ul class="list-unstyled">
            <?php
                while ($row = mysqli_fetch_assoc($select_sidebar))
                {
                    $title = $row['title'];
                    $id = $row['id'];
                    echo "<li><a href='category.php?category=$id'>{$title}</a></li>";
                }
            ?>
            </ul>
        </div>
    </div>
</div>
<?php
   include "widget.php"; 
?>