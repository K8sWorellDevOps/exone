<?php

use PHPMailer\PHPMailer\Exception;

class Notification
{
    static $email = "tobiastrapp23@gmail.com";

    function send_email($mail, $sub, $con)
    {
        try{
            $to = $mail;
            $subject = wordwrap($sub, 70);
            $content = $con;
            $header = "FROM: ".self::$email;
    
            mail($to,$subject,$content,$header);
        }catch(Exception $e){
            throw new Exception($e, error_log("Email could not send!"));
        }
    }

    private static function getEmail(){
        return $email;
    }
}